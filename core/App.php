<?php

namespace core;


class App
{
    protected $_config;

    public function __construct()
    {
    }

    /**
     * 初始化操作
     */
    private function init()
    {
        //框架默认utf8
        header("Content-type: text/html; charset=utf-8");
        //时区
        date_default_timezone_set('Asia/Shanghai');
        //常用的常量处理
        define('DS', DIRECTORY_SEPARATOR);
        //APP路径
        define('APP_PATH' ,dirname(__DIR__) . DS);
        // 开启调试模式
        define('APP_DEBUG', true);
        //自动加载处理
        require_once APP_PATH . DS . 'core' . DS . 'Autoloader.php';
        spl_autoload_register('\\core\\Autoloader::init');
        //关闭PHP自己报错,自己定义报错信息
        error_reporting(0);
        //debug
        //当我们的脚本执行完成或意外死掉导致PHP执行即将关闭时,我们的这个函数将会 被调用
        register_shutdown_function('\\core\\Debug::systemErrorHandler');
        //指定出错时指向我们自定义的错误句柄处理函数
        set_error_handler('\\core\\Debug::errorHandler');


    }

    public function run()
    {
        //初始化配置信息
        $this->init();

        Config::set(require(APP_PATH . 'config' . DS .'config.php'));
        //加载帮助函数
        require_once APP_PATH . DS . 'core' . DS . 'helpers.php';
        require_once APP_PATH . DS . 'app' . DS . 'Common' . DS . 'functions.php';
        $this->removeMagicQuotes();// 检测敏感字符并删除
        $this->unregisterGlobals();// 检测自定义全局变量并移除
        Route::run();
    }


    // 检测开发环境
    public function setReporting()
    {
        if (APP_DEBUG === true) {
            error_reporting(E_ALL);
            ini_set('display_errors','On');
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors','Off');
            ini_set('log_errors', 'On');
        }
    }

    // 删除敏感字符
    public function stripSlashesDeep($value)
    {
        $value = is_array($value) ? array_map(array($this, 'stripSlashesDeep'), $value) : trim(stripslashes($value));
        return $value;
    }

    // 检测敏感字符并删除
    public function removeMagicQuotes()
    {
        if (get_magic_quotes_gpc()) {
            $_GET = isset($_GET) ? $this->stripSlashesDeep($_GET ) : '';
            $_POST = isset($_POST) ? $this->stripSlashesDeep($_POST ) : '';
            $_COOKIE = isset($_COOKIE) ? $this->stripSlashesDeep($_COOKIE) : '';
            $_SESSION = isset($_SESSION) ? $this->stripSlashesDeep($_SESSION) : '';
        }
    }

    // 检测自定义全局变量并移除。因为 register_globals 已经弃用，如果
    // 已经弃用的 register_globals 指令被设置为 on，那么局部变量也将
    // 在脚本的全局作用域中可用。 例如， $_POST['foo'] 也将以 $foo 的
    // 形式存在，这样写是不好的实现，会影响代码中的其他变量。 相关信息，
    // 参考: http://php.net/manual/zh/faq.using.php#faq.register-globals
    public function unregisterGlobals()
    {
        if (ini_get('register_globals')) {
            $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
            foreach ($array as $value) {
                foreach ($GLOBALS[$value] as $key => $var) {
                    if ($var === $GLOBALS[$key]) {
                        unset($GLOBALS[$key]);
                    }
                }
            }
        }
    }


}