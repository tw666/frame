<?php

namespace nsms;

class Nsms
{

    public $userid;
    public $account;
    public $password;
    public $conttent_pre;

    public function __construct($userid, $account, $password, $content_pre)
    {
        $this->userid = $userid;
        $this->account = $account;
        $this->password = $password;
        $this->content_pre = $content_pre;
    }

    /**
     * 发送短信
     * @param type $to
     * @param type $content
     * @return type
     */
    public function sendSms($to, $content = '')
    {
        $data = [
            'action'   => 'send',
            'userid'   => $this->userid,
            'account'  => $this->account,
            'password' => $this->password,
            //短信内容
            'content'  => $content . $this->conttent_pre,
            //手机号码
            'mobile'   => $to,
            'sendTime' => '',
            'extno'    => ''
        ];
        $data['content'] = urlencode($data['content']);
        //拼接参数
        $str = '';
        foreach ($data as $k => $v) {
            $str .= '&' . $k . '=' . $v;
        }
        $str = ltrim($str, "&");

        $url = $param['sms_config']['sms_server'] . "?" . $str;
        // \core\Log::set($url);
        $output = self::getCurl($url);
        $result = simplexml_load_string($output);
        $result = self::object_array($result);

        if ($result['returnstatus'] == 'Success') {
            return $msg = [
                'status' => true,
                'msg'    => '发送成功'
            ];
        } else {
            \core\Log::set('短信发送失败:' . $to . "\t" . $result['message']);
            return $msg = [
                'status' => false,
                'msg'    => $result['message']
            ];
        }
    }

    //对象转换数组
    static function object_array($array)
    {
        if (is_object($array)) {
            $array = (array) $array;
        } if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = self::object_array($value);
            }
        }
        return $array;
    }

    /**
     * 请求方法
     * @param unknown $url  请求地址
     */
    public function getCurl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}
