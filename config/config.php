<?php
return [

    'debug' => 'true',

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    */

    'DB'=>[
        'mysql'=>[
            'dsn'        => 'mysql:host=127.0.0.1;dbname=tang-frame;charset=utf8',
            'charset'    => 'utf8',
            'username'   => 'root',
            'password'   => 'root',
            'prefix'     => 'php34_',
        ]
    ],

    'defaultController' =>'Index',
    'defaultAction'     =>'index',
];

