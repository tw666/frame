<?php


namespace core;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\JsonFormatter;

class Log
{

    protected static $loginfo = [];
    protected static $logObj;

    public static function set($msg = '', $error = false, $file_name = '', $write_view = true)
    {
        self::$loginfo[] = $msg;
        self::info($msg, ['message' => $msg, 'trace' => debug_backtrace()]);
    }

    /**
     *  获取monolog 对象
     * @param string $file_name
     * @return object
     */
    public static function getObj($file_name = '')
    {
        if (!self::$logObj) {
            if (empty($file_name)) {
                $file_name = APP_PATH . DS . 'log' . DS . date('Y-m') . DS . date('Ymd') . ".log";
            }
            $log = new Logger('FishPHP');
            $s = new StreamHandler($file_name);
            $t = new \Monolog\Formatter\JsonFormatter();
            $s->setFormatter($t);
            $log->pushHandler($s);
            self::$logObj = $log;
        }
        return self::$logObj;
    }

    /**
     * 紧急状况，比如系统挂掉
     * 
     * @param type $msg
     * @param type $context
     */
    public static function emergency($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context=self::doTrace($context);
        self::getObj()->addEmergency($msg, $context);
    }

    /**
     * 处理 trace
     * @param type $context
     * @return type
     */
    public static function doTrace($context = [])
    {
        if (empty($context['trace'])) {
            return $context;
        }
        $t = [];
        foreach ($context['trace'] as $k => $v) {
            $t[] = [
                'line'     => !empty($v['line']) ? $v['line'] :'',
                'file'     => !empty($v['file']) ? $v['file'] :'',
                'function' => $v['function'],
                'args'     => $v['args']
            ];
        }
        $context['trace'] = $t;
        return $context;
    }

    /**
     * 需要立即采取行动的问题，比如整站宕掉，数据库异常等，这种状况应该通过短信提醒
     * @param type $msg
     * @param type $context
     */
    public static function alert($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context = self::doTrace($context);
        self::getObj()->addAlert($msg, $context);
    }

    /**
     * 严重问题，比如：应用组件无效，意料之外的异常
     * 
     * @param type $msg
     * @param type $context
     */
    public static function critical($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context = self::doTrace($context);
        self::getObj()->addCritical($msg, $context);
    }

    /**
     * 运行时错误，不需要立即处理但需要被记录和监控
     * @param type $msg
     * @param type $context
     */
    public static function error($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context = self::doTrace($context);
        self::getObj()->addError($msg, $context);
    }

    /**
     * 警告但不是错误，比如使用了被废弃的API
     * 
     * @param type $msg
     * @param type $context
     */
    public static function warning($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context = self::doTrace($context);
        self::getObj()->addWarning($msg, $context);
    }

    /**
     * 普通但值得注意的事件
     * 
     * @param type $msg
     * @param type $context
     */
    public static function notice($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context = self::doTrace($context);
        self::getObj()->addNotice($msg, $context);
    }

    /**
     * 感兴趣的事件，比如登录、退出
     * 
     * @param type $msg
     * @param type $context
     */
    public static function info($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context = self::doTrace($context);
        self::getObj()->addInfo($msg, $context);
    }

    /**
     * 详细的调试信息
     * 
     * @param type $msg
     * @param type $context
     */
    public static function debug($msg, $context)
    {
        if (!is_array($context)) {
            $context = [
                'message' => $context
            ];
        }
        $context = self::doTrace($context);
        self::getObj()->addDebug($msg, $context);
    }

    public static function show()
    {
        return self::$loginfo;
    }

}
