<?php

namespace core;

class Debug
{

    //错误记录
    protected static $loginfo = [];
    protected static $sql = [];

    /**
     * 错误提示
     * @return type
     */
    public static function systemErrorHandler()
    {
        $message = '';
        if ($e = error_get_last()) {
            $separator = "\r\n";
            $message .= "出错文件:" . $e['file'] . $separator;
            $message .= "出错行数:" . $e['line'] . $separator;
            $message .= "错误信息:" . $e['message'] . $separator;
            $title = 'tang-PHP Error';
//            Log::alert('Error', $e);
            $message = str_replace($separator, '<br />', $message);
            $message = str_replace('#', '<br />#', $message);
            $color = '#9C191F';
        } else {
            if (empty(self::$loginfo)) {
                return;
            }
            foreach (self::$loginfo['message'] as $k => $v) {
                Log::notice('Notice', [
                    'file'    => self::$loginfo['errfile'][$k],
                    'line'    => self::$loginfo['errline'][$k],
                    'message' => self::$loginfo['message'][$k],
                    'type'    => self::$loginfo['type'][$k],
                ]);
               // Log::set($v . "\t" . self::$loginfo['errfile'][$k] . ":" . self::$loginfo['errline'][$k]);
                $message .= '<p>Error:' . $v . '<br>';
                $message .= 'File:' . self::$loginfo['errfile'][$k] . ' Line: ' . self::$loginfo['errline'][$k] . '</p><br>';
            }
            $title = 'tang-PHP Notice';
            $color = '#337AB7';
        }
        if (!empty($message)) {
            $tpl = '<p></p><div style="display:inline-block;font-family: 微软雅黑;line-height: 1.2em;;border:1px solid #E8E8E8;padding:10px">';
            $tpl .= '<h3 style="font-weight:100;margin:0;margin-bottom:20px;font-size: 16px;height:40px;line-height:40px;padding-left:20px;color:white;background-color:' . $color . ';">' . $title . '</h3>';
            $tpl .= '<div style="padding-left:10px;font-size:14px;">' . $message . '</div>';
            $tpl .= '</div>';
        }
        if (Config::get('debug')) {
            echo $tpl;
        }
    }

    /**
     * 非致命性错误捕捉
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline)
    {
        self::$loginfo['message'][] = $errstr;
        self::$loginfo['errfile'][] = $errfile;
        self::$loginfo['errline'][] = $errline;
        self::$loginfo['type'][] = $errno;
    }

    /**
     * console debug
     */
    public static function debugIng($return = false)
    {
        $has_debug = Request::get('get', 'debug');
        if (Config::get('debug')) {
            /**
             * 执行时间处理
             */
            $etime = microtime(true);
            $total_time = $etime - FISH_RUN_START;
            /**
             * debug
             */
            $debug = [];
            $debug['debug'] = 'FishPHP Debug';
            $debug['runtime'] = round($total_time, 4) . 's';
            $debug['_COOKIE'] = $_COOKIE;
            $debug['_SESSION'] = $_SESSION;
            $debug['included_files'] = get_included_files();
            $debug['_SERVER'] = $_SERVER;
            $debug['_POST'] = $_POST;
            $debug['_GET'] = $_GET;
            $debug['_REQUEST'] = $_REQUEST;
            $debug['_FILES'] = $_FILES;
            $debug['SQL'] = self::$sql;
            if ($return) {
                return $debug;
            }
            $e = '<script type="text/javascript">';
            $e .= "console.log('-------FishPHP Debug Start-------');";
            $e .= "console.log(" . json_encode($debug) . ");";
            $e .= "console.log('-------FishPHP Debug End-------');";
            $e .= '</script>';
            echo $e;
        }
    }

    /**
     * 记录 sql
     * @param string $sql
     */
    public static function setSql($sql)
    {
        self::$sql[] = $sql;
    }

}
