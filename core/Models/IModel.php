<?php
/**
 * Date: 2017/9/12
 */
namespace core\Models;

interface IModel
{

    function findByWhere();

    //将转换成数组形式
    function asArray();

    //删除数据
    function delete();

    //增加sql条件
    function where();

    //查询一条记录
    function one();

    //字段筛选
    function select();

    //执行原始sql
    function query();

    //更新数据
    function update();

    //执行新增操作
    function insert();

    //执行批量新增操作
    function insertAll();

    //自动增加前缀表名
    function from();

    //完整名称表名
    function table();

    //执行操作前数据
    function data();

    //排序
    function order();

    //筛选数量
    function limit();

    //查询所有记录
    function all();

    //统计
    function count();

    //计算和
    function sum($field);

    //求平均值
    function avg($field);

    //求最大值
    function max($field);

    //求最小值
    function min($field);

    //递增操作
    function increment();

    //递减操作
    function decrement();

    //sql过滤
    function quote();
}
