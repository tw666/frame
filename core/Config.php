<?php


namespace core;

class Config
{

    protected static $setting = [];

    /**
     * 写入配置
     * @param type $value
     */
    public static function set($value)
    {
        self::$setting = $value;
    }

    /**
     * 读取配置
     * @param type $key
     * @return type
     */
    public static function get($key = null)
    {
        if (empty($key)) {
            return self::$setting;
        }
        return isset(self::$setting[$key]) ? self::$setting[$key] : null;
    }

    /**
     * 获取所有配置信息
     * @return type
     */
    public static function show()
    {
        return self::$setting;
    }

}
