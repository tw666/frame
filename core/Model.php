<?php

/**
 * =============================================================================
 *  [FishPHP] (C)2015-2099 XiaoYu Inc.
 *  This content is released under the Apache License, Version 2.0 (the "License");
 *  Licensed    http://www.apache.org/licenses/LICENSE-2.0
 *  Link        http://www.fishphp.com
 * =============================================================================
 * @author     Lxh<605918235@qq.com>
 * @created    2016-04-12
 *  core model
 * =============================================================================
 */

namespace core;

use core\Models\BasicModel;

class Model extends BasicModel {

}