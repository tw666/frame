<?php

namespace core;

class Db
{

    protected static $objInstance;

    private function __construct()
    {
        
    }

    public static function getInstance()
    {
        if (!self::$objInstance) {
            $database = Config::get('DB')['mysql'];
            self::$objInstance = new \PDO($database['dsn'], $database['username'], $database['password'], array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING));
            self::$objInstance->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            self::$objInstance->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, false);
        }

        return self::$objInstance;
    }

    function __clone()
    {
        
    }

}
