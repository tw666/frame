<?php

namespace nsession;

use core\Di;
use core\Request;
use core\Config;

class Session
{

    private $db;
    private $session_maxlifetime = 30;

    function __construct()
    {
        $this->db = Di::redis();
        session_set_save_handler(
                array(&$this, "session_open"), array(&$this, "session_close"), array(&$this, "session_read"), array(&$this, "session_write"), array(&$this, "session_destroy"), array(&$this, "session_gc")
        );
    }

    /**
     * @param $session_path
     * @param $session_name
     * @return bool
     */
    function session_open($session_path, $session_name)
    {
        return true;
    }

    /**
     * @return bool
     */
    function session_close()
    {
        return $this->session_gc($this->session_maxlifetime);
    }

    /**
     * @param $session_id
     * @return string
     */
    function session_read($session_id)
    {
        $session_id = 'sess_' . $session_id;
        $data = $this->db->get($session_id);
        return $data ? $data : false;
    }

    /**
     * @param $session_id
     * @param $data
     * @return bool
     */
    function session_write($session_id, $data = '')
    {
        $session_id = 'sess_' . $session_id;
        $this->db->set($session_id, $data);
        $this->db->expire($session_id, $this->session_maxlifetime);
        return true;
    }

    /**
     * @param $session_id
     * @return bool
     */
    function session_destroy($session_id)
    {
        $session_id = 'sess_' . $session_id;
        return $this->db->del($session_id);
    }

    /**
     * @param $session_maxlifetime
     * @return bool
     */
    function session_gc($session_maxlifetime)
    {
        return true;
    }

}
