<?php

/**
 * =============================================================================
 *  [FishPHP] (C)2015-2099 XiaoYu Inc.
 *  This content is released under the Apache License, Version 2.0 (the "License");
 *  Licensed    http://www.apache.org/licenses/LICENSE-2.0
 *  Link        http://www.fishphp.com
 * =============================================================================
 *  @author     Tangqian<tanufo@126.com> 
 *  @created    2015-10-10
 *  自动引入文件
 * =============================================================================                   
 */

namespace core;

class Autoloader
{

    /**
     * 路径数组 保持唯一
     * @var type 
     */
    private static $file_array = [];

    public static function init($class_name)
    {

        /**
         * 框架内部默认后缀为
         */
        $file = APP_PATH . str_replace('\\', DS, $class_name) . '.php';
        /**
         * vendor
         */
        $file2 = APP_PATH . DS . 'vendor' . DS . str_replace('\\', DS, $class_name) . '.php';
        /**
         * 作hash数组 存在直接返回 保证引入的文件唯一
         */
        $hash = md5($file);
        if (isset(self::$file_array[$hash])) {
            return;
        }
        if (file_exists($file)) {
            self::$file_array[$hash] = $file;
            require $file;
        } elseif (file_exists($file2)) {
            $hash = md5($file2);
            self::$file_array[$hash] = $file2;
            require $file2;
        } else {
            exit($class_name . '类不存在');
//            Log::alert('Autoloader', 'File ' . str_replace('\\', DS, $class_name) . '.class.php. does not exist.');
        }
    }

}
