<?php

namespace core;


class Route
{
    protected static $param;
    // 路由处理
    public static function run()
    {
        $controllerName = Config::get('defaultController');
        $actionName = Config::get('defaultAction');
        $param = array();

        $url = $_SERVER['REQUEST_URI'];
        // 清除?之后的内容
        $position = strpos($url, '?');
        $url = $position === false ? $url : substr($url, 0, $position);
        // 删除前后的“/”
        $url = trim($url, '/');

        if ($url) {

            // 使用“/”分割字符串，并保存在数组中
            $urlArray = explode('/', $url);
            // 删除空的数组元素
            $urlArray = array_filter($urlArray);
            // 获取控制器名
            $dir = ucfirst($urlArray[0]);

            $fileDir = APP_PATH . 'app' . DS . 'Controller' . DS . $dir;
            if(is_dir($fileDir)){
                // 获取控制器名
                $controllerName = !empty($urlArray[1]) ? ucfirst($urlArray[1]) : $controllerName;
                $controllerName = $dir . DS . $controllerName;
                //动作名
                $actionName = !empty($urlArray[2]) ? $urlArray[2] : $actionName;

            }else{
                // 获取控制器名
                $controllerName = ucfirst($urlArray[0]);
                array_shift($urlArray);
                $actionName = $urlArray ? $urlArray[0] : $actionName;
            }

            // 获取URL参数
            array_shift($urlArray);
            $param = $urlArray ? $urlArray : array();

            self::$param = $_REQUEST;

        }

        // 判断控制器和操作是否存在 并带上命名空间

        $controllerName = 'app\Controller\\'.$controllerName;
        $controller = $controllerName . 'Controller';

        $actionName = 'action'.ucfirst($actionName);

        if (!method_exists($controller, $actionName)) {
            exit($actionName . '方法不存在');
        }

        // 如果控制器和操作名存在，则实例化控制器，因为控制器对象里面
        // 还会用到控制器名和操作名，所以实例化的时候把他们俩的名称也
        // 传进去。结合Controller基类一起看
        $dispatch = new $controller($controllerName, $actionName);
        // $dispatch保存控制器实例化后的对象，我们就可以调用它的方法，
        // 也可以像方法中传入参数，以下等同于：$dispatch->$actionName($param)
        call_user_func_array(array($dispatch, $actionName), $param);
    }



}