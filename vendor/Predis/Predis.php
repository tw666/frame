<?php

/**
 * =============================================================================
 *  [FishPHP] (C)2015-2099 XiaoYu Inc.
 *  This content is released under the Apache License, Version 2.0 (the "License");
 *  Licensed    http://www.apache.org/licenses/LICENSE-2.0
 *  Link        http://www.fishphp.com
 * =============================================================================
 * @author     Tangqian<tanufo@126.com>
 * @created    2017-09-04
 * =============================================================================
 */

namespace Predis;

use Predis\Client;
use core\Config;

/**
 * Predis 采用单例模式操作
 */
class Predis
{

    protected static $objInstance;

    private function __construct()
    {
        
    }

    public static function getInstance()
    {
        if (!self::$objInstance) {
            self::$objInstance = new Client(Config::get('params')['redis']);
        }

        return self::$objInstance;
    }

    public static function __callStatic($name, $arguments)
    {
        return self::getInstance()->$name(...$arguments);
    }

    function __clone()
    {
        
    }

}
